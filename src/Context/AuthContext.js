import React, { useContext, useEffect, useState } from 'react';
import 'firebase/auth';
import { useHistory } from "react-router";
import firebase, { auth, db, storage } from '../firebase';
// import { getAuth, signInWithCustomToken } from "firebase/auth";


const AuthContext = React.createContext();

export function useAuth() {
    return useContext(AuthContext)
}

export function AuthProvider({ children }) {

    const [currentUser, setCurrentUser] = useState([]);
    // const [userInfo, setUserInfo] = useState([]);
    const [users, setUsers] = useState([]);
    const [loggedIn, setLoggedIn] = useState(false)
    let history = useHistory();

    // const [FiretoreUsers, setFirestoreUsers] = useState([]);

    function login(email, password){
       return auth.signInWithEmailAndPassword(email, password);
    }    

    const firestoreLogin = async (email) => {
        await db.collection("Users").where("email", "==", email)
        .get()
        .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                // doc.data() is never undefined for query doc snapshots
                console.log(doc.data());
                setUsers(doc.data())
                localStorage.setItem('loggedIn', true);
                console.log("Success")
                history.push('/profile')
            });
        }).catch(console.log('no such user exists'))
    }

    // const verify = () => {
    //     if(users){
    //         console.log(users)
    //         setLoggedIn(true);
    //         localStorage.setItem('loggedIn', true);
    //         // history.push('/profile')
    //     }else{
    //         setLoggedIn(false);
    //         localStorage.setItem('loggedIn', false);
    //     }
    // }

    const uploadData = (email, uid) => {
        db.collection('Users')
        .add({
            email: email,
            uid: uid
        }).then(() => {
            console.log('Data submitted!')
        }).catch((error) => {
            console.log(error.message)
        })
    }

    // console.log(currentUser)
    const signInWIthCUstomToken = (token) => {
        return auth.signInWithCustomToken(token)
    }

    const signUp = (email, password) => {
        return auth.createUserWithEmailAndPassword(email, password)
    }

    const logOut = () => {
        return auth.signOut();
    }

    useEffect(() => {
         const unsubscribe = auth.onAuthStateChanged(users => {
            setUsers(users);
            // getUsers();
        })
        
        return unsubscribe
    }, [])

    
    const value = {
        currentUser,
        logOut,
        signUp,
        signInWIthCUstomToken,
        login,
        uploadData,
        firestoreLogin,
        users
    }

    return (
        <AuthContext.Provider value={value} >
            {children}
        </AuthContext.Provider>
    )
}
