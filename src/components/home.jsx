import "../css/home.css";
import { Link } from "react-router-dom";
function Home() {
    return (

<div className="container">
  
<div className="l-container">

<Link to="/play" className="small-image a">
  <div className="b-game-card">
    <div className="b-game-card__cover" style={{backgroundImage: "url(https://andrewhawkes.github.io/codepen-assets/steam-game-cards/game_1.jpg)"}}></div>
  </div>
  
</Link>
<Link to="/play" className="small-image a">
  <div className="b-game-card">
    <div className="b-game-card__cover" style={{backgroundImage: "url(https://andrewhawkes.github.io/codepen-assets/steam-game-cards/game_2.jpg)"}}></div>
  </div>
  </Link>
<Link to="/play" className="small-image a">
  <div className="b-game-card">
    <div className="b-game-card__cover" style={{backgroundImage: "url(https://andrewhawkes.github.io/codepen-assets/steam-game-cards/game_3.jpg)"}}></div>
  </div>
  </Link>
<Link to="/play" className="small-image a">
  <div className="b-game-card">
    <div className="b-game-card__cover" style={{backgroundImage: "url(https://andrewhawkes.github.io/codepen-assets/steam-game-cards/game_4.jpg)"}}></div>
  </div>
  </Link>
</div>
  
</div>
    );
  }
  
  export default Home;