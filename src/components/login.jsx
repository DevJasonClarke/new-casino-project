import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { useAuth } from "../Context/AuthContext";
import "../css/login.css";
// import firebase from '../firebase';

function Login() {

  //signup credentials
  const [email, setEmail] = useState(window.localStorage.getItem("Email for signin") || "");
  // const [email, setEmail] = useState("");
  const [token, setToken] = useState("")
  const [password, setPassword] = useState("");
  const [error, setError] = useState('');
  const { login, currentUser, uploadData, firestoreLogin } = useAuth();
  const [loading, setLoading] = useState(false)
  //signup credentials

  //upload signup data
  // const [uploadEmail, setUploadEmail] = useState([]);
  // const [uploadUid, setUploadUid] = useState([]);

  //upload signup data


  let history = useHistory();

  const handleLogin = async () => {
    try{
      setError("")
      await login(email, password)
      // handleUploadData();
      // history.push('/profile')
    }catch{
      setError('Failed to login')
      console.log(error)
    }
    
    // console.log(currentUser)
  }

  const handleFirestoreLogin = async () => {
    try{
      // setError('');
        await firestoreLogin(email);
        // history.push('/profile')
        // console.log('success')
    }catch{
      setError('Failed to login')
      console.log(error)
    }

  }



  // const handleUploadData = async () => {
  //   await uploadData(email, password)
  // }


    return (
      <main className="panel">
        {/* <p>{apiResponse}</p> */}
        {/* <form action="" onSubmit={handleLogin}> */}
        <div className="panel__half half--first">

<h2>Login</h2>


<p>Login with your email account</p>
<div className="input">
            <input type="text" placeholder="Email" 
              className="input-text"
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              id="parentNumber"
              title="Email Address"
            />
       
              
   <button type="submit"  onClick={handleFirestoreLogin} className="button">
                  Login
                </button>
                </div>
                </div>
                <div className="panel__half half--second">
        
        <h2>Hello, friend!</h2>
        <p>Enter your personal details and start your journey with us</p>
    
        <button>Sign up</button>
      </div>

                {/* <div className="input">
                  <input
                    className="input-text"
                    name="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    id="parentNumber"
                    type='password'
                    placeholder="********"
                    title="Password"
                
                  />

                  <label className="input-label">Password</label>
                </div> */}


                {/* <div className="input">
                  <input
                    className="input-text"
                    name="password"
                    value={token}
                    onChange={(e) => setToken(e.target.value)}
                    id="parentNumber"
                    type='password'
                    placeholder="Token"
                    title="Password"
                
                  />

                  <label className="input-label">Token</label>
                </div> */}


                
              
                {/* <button type="submit" onClick={handleSingInWithToken} className="button">
                  Login Token
                </button> */}

                {/* <button type="submit" onClick={handleSendEmailLink}className="button">
                  SEND LINK
                </button>

                <button type="submit" onClick={handleSingInWithEmailLink}className="button">
                  SIGN IN WITH LINK
                </button> */}
    
        {/* </form> */}
       </main>
    );
  }
  
  export default Login;
  