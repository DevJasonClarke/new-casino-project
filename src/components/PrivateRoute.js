import React from 'react';
import { Redirect, Route, Router } from 'react-router-dom';
import { useAuth } from '../Context/AuthContext';

export default function PrivateRoute({ component: Component, ...rest }) {

    const { currentUser } = useAuth();
    const { users } = useAuth();

    return (
        <div>
            <Route {...rest}
                   render={props => {
                    return users ? <Component {...props} /> : <Redirect to='/login' />
                   }}
            >

            </Route>
        </div>
    )
}
